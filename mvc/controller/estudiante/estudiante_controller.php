<?php
require_once "../../model/BindParam.php";
require_once "../../model/query_database.php";

class estudiante_controller
{

    public function insertarEstudiante($nombre, $cedula, $nacimiento ,$seccion_ano,$contactos){
        $resultado = null;
        $binParam = new BindParam();
        $query = "call sp_insertarEstudiante(?,?,?,?)";// revisar  porque no inserta el nacimiento
        $binParam->add('s',$nombre); 
        $binParam->add('s', $cedula);
        $binParam->add('s',$nacimiento); 
        $binParam->add('i', (int)$seccion_ano);
     
        $repuesta = (query_database::find($query, $binParam));

        $repuesta = ($repuesta[0])['id'];
     

        if ( $repuesta!= 0 ) {  
            
            foreach ( (array) $contactos as $contacto) // solo esta insertando 1 
            {
                
                unset($binParam );
                $binParam = null;
                $binParam = new BindParam();
                
                $tipo =($contacto['tipo'] != 'CORREO')?  1 :  2;
                $query = "call sp_insertarContacto(?,?,?)";
                $binParam->add('i', $repuesta); 
                $binParam->add('s', $contacto['contacto']);
                $binParam->add('i', $tipo); 
              
             $resultado =  query_database::delete_update_insert($query, $binParam);
            }

        }else{
            $resultado ='todo mal';
        }
        echo json_encode( $resultado );
    
    }

    
    public function updateEstudiante($id_estudiante,$nombre, $cedula, $nacimiento ,$seccion_ano){
       
        $binParam = new BindParam();
        $query = "call sp_actualizarEstudiante(?,?,?,?,?)";
        $binParam->add('i',$id_estudiante); 
        $binParam->add('s',$nombre); 
        $binParam->add('s', $cedula);
        $binParam->add('s',$nacimiento); 
        $binParam->add('i', (int)$seccion_ano);
     
      echo  json_encode (query_database::delete_update_insert($query, $binParam)); // revisar esta respuesta por que actualiza y estatus es 0 ver como manipular stats desde la vase de datos
    }


    public function insertContacto($id , $contacto, $tipo)
    {   
        $binParam = new BindParam();   
        $query = "call sp_insertarContacto(?,?,?)";
        $tipo_contacto =($tipo!= 'CORREO')?  1 :  2;
        $binParam->add('i', $id); 
        $binParam->add('s', $contacto);
        $binParam->add('i', $tipo_contacto); 
        echo json_encode(query_database::delete_update_insert($query, $binParam));
    }


    public function eliminarContactoEstudiante($id)
    {
        $query = "call db_liceo_web.sp_eliminarContacto(?);";
        $binParam = new BindParam();
        $binParam->add('i',$id); 
        echo json_encode(query_database::delete_update_insert($query,$binParam) );
    }


    public function cargarGrados()
    {
        $query = "call db_liceo_web.sp_obtenerGrados();";
        $grados =query_database::findAll($query) ;
        echo json_encode( $grados);
    }
    
    
    public function cargarlistaEstudiantes()
    {
        $query = "call db_liceo_web.sp_obtenerEstudiantes();";
        echo json_encode(query_database::findAll($query) );
    }


    
    public function cargarDatosEstudiante($id)
    {
        $query = "call db_liceo_web.sp_obtenerDatosEstudiante(?);";
        $binParam = new BindParam();
        $binParam->add('i',$id); 
        echo json_encode(query_database::find($query,$binParam) );
    }
    
    public function cargarContactosEstudiante($id)
    {
        $query = "call db_liceo_web.sp_obtenerContactosEstudiante(?);";
        $binParam = new BindParam();
        $binParam->add('i',$id); 
        echo json_encode(query_database::find($query,$binParam) );
    }
    public function eliminarEstudiante($id)
    {
        $query = "call db_liceo_web.sp_eliminarEstudiante(?);";
        $binParam = new BindParam();
        $binParam->add('i',$id); 
        echo json_encode(query_database::delete_update_insert($query,$binParam) );
    } 
    
    

}
