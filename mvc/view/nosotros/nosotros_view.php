<!doctype html>
<html lang="es">
<head>
    <?php require_once "../base/metadata.php"?>

    <link rel="stylesheet" href="../../public/css/dashboard.css">
    <title>Nosotros View</title>
</head>
<body>

<?php require_once "../base/navbarAdmin.php"?>

<div class="container-fluid">
    <div class="row">
        <?php require_once "../base/menuVertical.php"?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <!-- Aqui va el codigo de cada pagina-->
            <p>Aqui va el codigo de cada pagina</p>
        </main>
    </div>
</div>

</body>
</html>